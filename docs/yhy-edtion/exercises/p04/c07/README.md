# 第07章： 设备管理

1. 数据传输控制方式有哪几种？试比较它们的优缺点。  

>解：  
>  
>+ 程序控制方式
>   + 优点： 实现简单，无额外的硬件支持需求
>   + 缺点：
>       + CPU与设备串行工作
>       + 多台设备间串行工作
>       + 直接检测设备的状态来控制数据传输，无法发现和处理设备或其他硬件产生的错误（完全无中断机制情况下）
>+ 中断控制方式
>   + 优点： 提高了CPU的利用率
>   + 缺点：
>       + 多次中断： 以设备的数据缓冲寄存器（或数据缓冲区）为单位传输数据，一次传输任务可能发生多次中断
>       + 忙于处理中断： 大量的`IO device`均采用中断方式，将使`CPU`忙于处理中断，可能导致`CPU`无法响应中断而出现中断丢失现象
>+ `DMA`控制方式
>   + 优点：
>       + 传输以数据块为单位
>       + 仅在开始和结束需要`CPU`干预，整块数据的传送是在`DMA`控制器的控制之下完成
>   + 缺点：
>       + 多个数据块需多次传输
>       + `IO device`增多时，多个`DMA`控制器同时使用，可能会引起`main memory`地址冲突从而使控制过程复杂化
>+ 通道控制方式
>   + 优点： 把对一个数据块的读（写）干预减少到对一组数据块的读（写）干预
>   + 缺点： 需专用的、昂贵的、相当于小型`CPU`的通道控制器

2. 何为设备的独立性？如何实现设备的独立性？  

>解：  
>  
>设备独立性： 也称为设备无关性，指应用程序独立于具体使用的物理设备，用户使用逻辑设备名申请使用物理设备。
>  
>设备独立性的作用：
>  
>+ 提高资源利用率： 用户使用逻辑设备名申请使用某类物理设备。当系统中有多台该类型的设备时，系统可将其中的任意一台分配给请求进程，而不局限于某一台特定的设备，可显著的改善资源的利用率。  
>+ 易于实现重定向： 设备独立性使用户进程独立于设备的类型。如进行输出时，既可以使用显示终端，也可以使用打印机。有了这种独立性，易于实现`IO redirection`（输入/输出重定向）。
>  
>实现设备独立性的方法： `LUT(Logical Unit Table)`逻辑设备表记录逻辑设备与物理设备的映射关系，该表每项包含 **逻辑设备名** 、 **物理设备名** 、 **设备驱动程序入口地址** 三项，进程请求设备时，系统将进程请求的逻辑设备名，在`LUT`寻找相应的物理设备分配给该进程。

3. 什么是缓冲？为什么要引入缓冲？操作系统如何实现缓冲技术？  

> 解：  
>  
> 缓冲是在两个不同速度设备之间传输信息时，实现平滑传输过程的一种手段
>  
>引入缓冲的原因（缓冲解决的问题）：
>  
>+ 缓解CPU与IO设备之间的速度不匹配的矛盾
>+ 减少中断CPU的次数
>+ 提高CPU与IO设备之间的并行性
>
>实现缓冲的方法：
>  
>在计算机系统中，少部分采用硬缓冲方式实现，大部分采用软缓冲方式实现。软缓冲是指在`IO`过程中，设置一块区域用于临时存放`IO data`从而实现缓冲，这块区域一般在`main memory`中。当输入设备需要输入数据时，将数据传输到输入缓冲区，当`CPU`需要处理输入数据时，从输入缓冲区中取下数据进行处理并释放已处理的输入缓冲区空间，将`CPU`需要输出数据到输出设备时，`CPU`将数据输出到输出缓冲区，输出设备从输出缓冲区取下数据进行输出并释放已输出的输出缓冲区空间。

4. 设备分配中为什么可能出现死锁？

>解：  
>  
>为提高进行效率，`OS`一般支持进程申请并占有多个`IO`资源，当申请多个`IO`资源时，两个或两个以上进程产生循环等待`IO`资源时，即出现死锁。

5. 以打印机为例说明SPOOLing技术的工作原理。

>解：  
>  
>当用户进程请求使用打印机时，`SPOOLing`系统为之输出数据，但并不真正将打印机分配给该用户进程，工作原理和工作流程在大致如下：
>
>当用户进程请求打印输出时，打印服务进程（输出进程）接受用户的打印请求，但并不真正执行实际打印，而是为进程在输出井中分配一个空闲区，并将要打印的数据送入其中，同时为该用户进程生成一张请求打印表，将用户的打印要求填入其中，再将该表挂在请求打印队列上。如果还有进程要求打印输出，系统仍然可以以同样的方式接受请求。
>
>当打印机空闲时，打印服务进程从请求打印队列队首取下一张请求打印表，根据表中的需求将要打印的数据从输出井传送到内存的输出缓冲区，再由打印机进行打印。打印完毕后，打印服务进程再查看打印请求队列中是否还有请求打印表，若有，则再取一张请求打印表，并根据表中的需求进行打印，如此反复，直至打印请求队列空时，打印服务进程将自已阻塞，直至下次再有打印请求时被唤醒。

6. 假设一个磁盘有200个柱面，编号为0 ~ 199，当前存取臂的位置是在143号柱面上，并刚刚完成了125号柱面的服务请求，如果存在下列请求序列：86、147、91、177、94、150、102、175、130，试问：为完成上述请求，采用下列算法时存取的移动顺序是什么？移动总量是多少？  
（1） 先来先服务（FCFS）  
（2） 最短寻道时间优先（SSTF）  
（3） 扫描算法（SCAN）  
（4） 循环扫描算法（C-SCAN）  

> 解：  
>  
> `FCFS`
>
>+ 移动顺序： 143、86、147、91、177、94、150、102、175、130
>+ 移动总量： （143-86）+（147-86）+（147-91）+（177-91）+（177-94）+（150-94）+（150-102）+（175-102）+（175-130）= 565
>
> `SSTF`
>  
>+ 移动顺序：143、147、150、130、102、94、91、86、175、177
>+ 移动总量：（147-143）+（150-147）+（150-130）+（130-102）+（102-94）+（94-91）+（91-86）+（175-86）+（177-175）=162
>
> `SCAN`： 当前存取臂的位置是在143号柱面上，并刚刚完成了125号柱面的服务请求 = = > 当前扫描当前存取臂的位置是在143号柱面上，并刚刚完成了125号柱面的服务请求 = = > 当前方向是自低号向高号柱面方向是自低号向高号柱面
>  
>+ 移动顺序：143、147、150、175、177、130、102、94、91、86
>+ 移动总量：（147-143）+（150-147）+（175-150）+（177-175）+（177-130）+（130-102）+（102-94）+（94-91）+（91-86）=125
>
> `C-SCAN`： 当前存取臂的位置是在143号柱面上，并刚刚完成了125号柱面的服务请求 = = > 扫描方向是自低号向高号柱面
>  
>+ 移动顺序：143、147、150、175、177、86、91、94、102、130
>+ 移动总量：（147-143）+（150-147）+（175-150）+（177-175）+（177-86）+（91-86）+（94-91）+（102-94）+（130-102）=169

7. 磁盘的访问时间分成三部分：寻道时间、旋转时间和数据传输时间。而优化磁盘磁道上的信息分布能减少输入输出服务的总时间。例如，有一个文件有10个记录A,B,C,……,J存放在磁盘的某一磁道上，假定该磁盘共有10个扇区，每个扇区存放一个记录，安排如下表所示。现在要从这个磁道上顺序地将A~J这10个记录读出，如果磁盘的旋转速度为20ms转一周，处理程序每读出一个记录要花4ms进行处理。试问：  
（1） 处理完10个记录的总时间为多少？  
（2） 为了优化分布缩短处理时间，如何安排这些记录？并计算处理的总时间。  

|扇区号|1|2|3|4|5|6|7|8|9|10|
|--|--|--|--|--|--|--|--|--|--|--|
|记录号|A|B|C|D|E|F|G|H|I|J|

> 解：
>  
> 旋转每记录时间： 20ms / 10 = 2ms
> 
>+ 读取A记录： 2ms + 4ms = 6ms
>+ 读取完A记录后，磁头转至D记录，为了读取B记录需旋转8个扇区，因此，需时： 8 * 2ms + 2ms + 4ms = 22ms
>+ 剩余8条记录需时与B记录相同，因此，总需时： 6ms + 9 * 22ms = 204ms
>  
> 为了缩短总时间，应优化为刚读取处理完一条记录时，下一条记录刚好在当前磁头下，即每隔两个扇区存放下一条记录  
> 总需时： 10 * 6ms = 60ms

|扇区号|1|2|3|4|5|6|7|8|9|10|
|--|--|--|--|--|--|--|--|--|--|--|
|记录号|A|H|E|B|I|F|C|J|G|D|

8. 假设一个磁盘有100个柱面，每个柱面有10个磁道，每个磁道有15个扇区。当进程的要访问磁盘的12345扇区时，计算该扇区在磁盘的第几柱面、第几磁道、第几扇区？

> 解：  
>  
> 每个柱面的扇区数： 10 * 15 = 150  
> 12345 / 150 = 90, 12345 % 150 = 24, 24 /15 = 1, 24 % 15 = 9  
> 12345位于90号柱面、1号磁道、9号扇区  

9. 一个文件记录大小为32B，磁道输入输出以磁盘块为单位，一个盘块的大小为512B。当用户进程顺序读文件的各个记录时，计算实际启动磁盘I/O占用整个访问请求时间的比例。

> 解：  
>  
> 512 / 32 = 16 = > 一次读取实际读取了16条记录 = > 实际IO占比 ： 1/16 = 6.25%

10. 如果磁盘扇区的大小固定为512B，每个磁道有80个扇区，一共有4个可用的盘面。假设磁盘旋转速度是360rpm。处理机使用中断驱动方式从磁盘读取数据，每字节产生一次中断。如果处理中断需要2.5ms，试问：  
（1） 处理机花费在处理I/O上的时间占整个磁盘访问时间的百分比是多少（忽略寻道时间）？  
（2） 采用DMA方式，每个扇区产生一次中断，处理机话费在处理I/O上的时间占整个磁盘访问时间的百分比又是多少？

>解：  
>
>题中未给出的信息：  
>
>+ 寻道的相关信息： 忽略寻道时间
>+ 块与扇区的关系： 磁盘的物理单位是扇区，`OS`分配、读写磁盘的逻辑单位是块，一个块由一个或多个扇区组成，由于题中未给出块与扇区的对应关系，因此，假定一个块由一个扇区组成，即最少读取一个扇区
>+ 顺序读还是随机读： 假定为随机读，即每次都要旋转待时间
>+ 磁盘控制器缓冲区大小： 假定为一个扇区
>
>占比计算：  
>
>+ 磁盘旋转一周时间： 60/360 = 1/6s
>+ 访问一个扇区时间： 1/6 * 1/80 = 1/480s
>+ 磁盘访问时间： 1/2 * 1/6 + 1/480 = 1/ 96 s = 0.0104s
>+ 中断方式
>    + CPU花费时间： 512 * 2.5ms = 1280ms = 1.28 s
>    + CPU时间占比： 1.28 / (1.28 + 0.0104) = 1.28 / 1.2904 = 99.19%
>+ `DMA`方式
>    + CPU花费时间： 2.5 ms = 0.0025s
>    + CPU时间占比： 0.0025 / (0.0025 + 0.0104) = 0.0025 / 0.0129 = 19.38%
