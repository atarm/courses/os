# `Linux`背景

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

- [UNIX](#unix)
- [Linux](#linux)
- [OSS](#oss)

<!-- /code_chunk_output -->

## UNIX

1. 1968，`Multics`项目
1. 1970，`Unix`（`Assembly`）： "1970/01/01"是`Unix元年`，也是时间戳的起点
1. 1973，`Unix`（`C`）：
1. 1975，`BSD`：

## Linux

1. 1991，`v0.0.1`：`Linus's Unix`>>>`Linux`>>>`Linux Is Not Unix X`

## OSS

1. 1983/09，`Richard Stallman`创建`GNU`项目，并在次年启动试图创建一个自由的操作系统
1. 1985/10，`FSF(Free Software Foundation)`成立，支持开源运动和开源许可协议
1. 1989/01，`FSF`发布`GPL v1`
1. 1990，`Emacs`, `GCC`
1. 1991，`Phil Zimmerman`开源高强度加密软件`PGP`受到美国政府的管制调查
1. 1991， `Linux`加入`GNU`
1. 1992， `GNU/Linux`
1. 1995，`Apache HTTP Server`发布
1. 1997，`Eric Raymond`发表《大教堂与集市》
1. 1998/01，`Netscape`公司公开`Netscape`浏览器和`Bugzilla`的源码，`Mozilla`开源项目启动
1. 1998/02，`Bruce Perens`和`Eric Raymond`等成立`OSI(Open Source Initiative, 开源软件促进会)`
1. 1998/04，`Open Source Summit`（`OSCON(O'Reilly Open Source Convention)`的前身）首届举行
1. 1999/01，`SourceForge`上线
1. 1999/03，`Apache软件基金会`成立
1. 1999/10，`Redhat`纳斯达克IPO成功
1. 2000，美国法院宣判软件源码是言论自由，受宪法第一修正案的保护
1. 2000，`MySQL`采用`GPL`开源
1. 2001/01，`Wikipedia`上线
1. 2001，`IBM`投资一亿美元给`Linux`构建商业生态
1. 2002，`MediaWiki`发布，成立`Wikipedia基金会`
1. 2004/01，`Eclipse基金会`成立
1. 2004/01，`Xen 1.0`发布
1. 2004/11，`Mozilla`项目发布`Firefox v1.0`
1. 2005，`Linus Torvalds`发布`Git`
1. 2005，`Hadoop`作为`Nutch`的一部分正式引入到`Apache基金会`
1. 2007/02，`Hadoop`正式独立成为`Apache`的顶级项目
1. 2007/06，`FSF`发布`GPL v3`
1. 2007/10，`XenServer`被`Citrix`收购
1. 2007/11，`Google`推出`Android`开源项目`AOSP`
1. 2007，`Linux基金会`成立
1. 2008/01，`Sun`以10亿美金收购`MySQL AB`公司
1. 2008/04，`GitHub`上线
1. 2008/08，`SourceForge`拥有18万开源项目，190万注册用户
1. 2008/10，_Bitcoin: A Peer-to-Peer Electronic Cash System_ 发表
1. 2008/10，`Cloudera`成立并发行第一个`Hadoop`集成版本`CDH`
1. 2009/02，`MongoDB`发布
1. 2009/04，`Oracle`以74亿美元收购`Sun`
1. 2010，`Apache许可协议2.0`发布
1. 2010/07，`NASA`和`Rackspace`在`OSCON 2010`上宣布合作开发`OpenStack`，3个月后发布了`Austin`版本
1. 2012/09，`OpenStack基金会成立`，设置技术委员会、用户委员会和董事会
1. 2013，`dotCloud`公司开源`Docker`
1. 2014，`Hortonworks`成功上市，市值11亿美金
1. 2015/06/22，`Docker`发起`OCI(Open Container Initiative, 开源容器倡议)`（`Linux Foundation`项目）
1. 2015/07/10，`Kubernetes 1.0`发布，采用`Apache License v2.0`
1. 2015/07/21，`OSCON 2015`宣布发起`CNCF(Cloud Native Computing Foundation)`（`Linux Foundation`项目）
1. 2015/11，`Tensorflow`发布，采用`Apache License v2.0`
1. 2016，`Hyperledger`发布
1. 2017，`Cloudera`成功上市，市值41亿美元
1. 2017，量子计算`Qiskit`发布
1. 2018，`Microsoft`以75亿美元收购`GitHub`
1. 2018，`IBM`以340亿美元收购`Redhat`
1. 2018/10，`Cloudera`和`Hortoworks`宣布全股票对等合并，市值52亿美元
1. 2019/03，`xenserver.org`正式停用
1. 2019/11，`Mirantis`收购`Docker Inc.`
