# Part 05: File Management文件管理

- [1. Agenda](#1-agenda)
    - [1.1. Main Topics](#11-main-topics)
    - [1.2. Keys for Programming](#12-keys-for-programming)
    - [1.3. Terminologies](#13-terminologies)
- [What is File System文件系统是什么](#what-is-file-system文件系统是什么)
- [2. Functions of File System文件系统的功能](#2-functions-of-file-system文件系统的功能)
- [3. File and Directory文件和目录](#3-file-and-directory文件和目录)
- [4. Implement of File System文件系统的实现](#4-implement-of-file-system文件系统的实现)
- [5. Reliability of File System文件系统的可靠性](#5-reliability-of-file-system文件系统的可靠性)
- [6. Protection Mechanism of File System文件系统的保护机制](#6-protection-mechanism-of-file-system文件系统的保护机制)

## 1. Agenda

### 1.1. Main Topics

1. 文件系统的功能
1. 文件和目录
    1. 文年的类型、属性、操作和访问方式
    1. 文件的逻辑结构
    1. 目录的逻辑结构
1. 文件系统的实现
    1. 文件系统的格式
    1. 文件和目录的物理结构
    1. 空闲存储空间的管理
1. 文件系统的可靠性
1. 文件系统的保护机制

### 1.2. Keys for Programming

1. understanding `file`, `directory` and `path`
1. understanding differences of different `FS`
1. understanding `serialization` and `persistance`
1. understanding `DBMS` is based on `file`

### 1.3. Terminologies

1. `FS`(File System，文件系统): usually short for `FMS`(File Management System，文件管理系统)
1. `FCB`(File Control Block，文件控制块)
1. `NFS`(Network File System，网络文件系统)
1. `ACL`(Access Control List，访问控制列表)
1. `i-node`(index-node)
1. `path`: which can unique indicate a file location(include file name)

## What is File System文件系统是什么

文件系统是`OS`用来明确存储设备或分区上的文件的方法和数据结构，即在存储设备上组织文件的方法。操作系统中负责管理和存储文件信息的软件机构称为文件管理系统，简称文件系统。 文件系统由三部分组成：文件系统的接口，对对象操作和管理的软件集合，对象及属性。从系统角度来看，文件系统是对文件存储设备的空间进行组织和分配，负责文件存储并对存入的文件进行保护和检索的系统。具体地说，它负责为用户建立文件，存入、读出、修改、转储文件，控制文件的存取，当用户不再使用时撤销文件等。

>[内核与文件系统的关系.](https://zhuanlan.zhihu.com/p/584657032)

## 2. Functions of File System文件系统的功能

1. characteristics
    1. file system is considered part of most `OS kernel`文件系统通常作为`OS kernel`的一部分
    1. input to applications is by means of a file以文件为逻辑单位访问
    1. output is saved in a file for long-term storage持久存储
1. function requirements
    1. access file by file name按名访问/按名存取
    1. organize files and their meta-data组织文件及其元数据
    1. manage storage space管理存储空间
    1. sharing mechanism共享机制
1. non-function requirements
    1. performance性能
    1. effectiveness有效性
    1. reliability可靠性
    1. security安全性

## 3. File and Directory文件和目录

1. 文件： 具有文件名的一组相关数据的集合
    1. 文件数据
    1. 文件元数据： 存储于`FCB`文件控制块
        1. 基本信息
            1. 文件名
            1. 内部标识符
            1. 物理地址
        1. 文件存储控制信息
            1. 文件存取权限
            1. 用户存取权限
        1. 文件的使用信息
            1. 创立时间
            1. 修改时间
            1. 当前使用信息
1. 文件操作
    1. 逻辑操作： 对文件内容的应用级的操作，即，在知晓文件的逻辑结构下以逻辑结构为单位进行操作，`OS`并不关注文件的逻辑操作
    1. 物理操作： 即在不知晓文件的逻辑结构下进行的操作，即，文件的通用操作
1. 文件的访问方式
    1. 顺序访问
    1. 随机访问
1. 文件的逻辑结构： 应用程序或数据库系统关注，`OS`并不关注文件的逻辑结构
    1. unstructured无结构文件：also known as `pile file` or `stream file`
    1. structured结构文件： based on `record`
        1. `sequential`顺序文件： 固定格式、固定长度
        1. `indexed`索引文件： 索引指针指向具体记录
            1. `hash indexed`
            1. `B-tree indexed(Balance)`
            1. `R-tree indexed(Rectangle)`: indexing multi-dimensional information such as geographical coordinates, rectangles or polygons
        1. `indexed-sequential`索引顺序文件： 索引指针指向记录组
    1. semi-structured半结构文件
        1. `tree-structure`树型结构： 文件整体结构是树型结构，每个结点的结构可不一样，每个叶子结点的结构可不一样
1. 文件目录：
    1. directory itself is a file owned by the `OS`
    1. 逻辑上： 包含文件和其它目录的一种文件
        1. 解决（或缓解）命名冲突
        1. 组织文件和子目录
        1. 实现"按名存取"的关键
    1. 物理上： 存储`FCB`
1. `i-node`
    + 储存文件元数据（除文件名）的区域
    + `UNIX-like`内部使用`i-node`号标识文件
        1. 根据文件路径读取`i-node`号
        1. 根据`i-node`号读取文件信息
        1. 根据`i-node`文件信息中的物理地址，读取文件
    + 目录： 存储文件名和文件`i-node`号
1. 目录结构
    1. 单级目录结构： 一个文件系统一个目录
    1. 两级目录结构： 一个用户账户一个目录
    1. 多级目录结构/树形目录结构
        1. `current directory`当前目录/`working directory`工作目录
        1. `absolute path`绝对路径： 从根目录开始的路径
        1. `relative path`相对路径： 从当前目录开始的路径
1. 文件共享
    + local sharing
        + `hard-link`硬链接： 不同的文件路径指向同一个`i-node`
        + `soft-link`软链接： 文件的内容是另一个文件的路径
    + remote sharing
        + `NFS`
        + `internet`

>1. 数据库索引为什么使用B+树？： <https://www.jianshu.com/p/4dbbaaa200c4>
>1. 阮一峰 - 理解inode： <https://www.ruanyifeng.com/blog/2011/12/inode.html>
>1. 阮一峰 - Unix目录结构的来历： <https://www.ruanyifeng.com/blog/2012/02/a_history_of_unix_directory_structure.html>
>1. Linux树状目录结构图： <https://www.yinxiang.com/everhub/note/cbe9a279-c031-4cf8-81b1-4047fbd2fc14>

## 4. Implement of File System文件系统的实现

1. 文件系统： 实现存储、控制、访问文件的一套机制及其实现
    1. 文件
    1. 文件管理软件
    1. 接口
1. 格式化：
    1. 低级格式化： 将外存进行扇区的划分
    1. 高级格式化： 在外存分区中分块和建立文件系统
1. 文件存储管理
    1. 文件存取分配的基本单位： block块/cluster簇
    1. 文件存储空间的分配方式
        1. 连续分配
        1. 离散分配
            1. 链接分配
            1. 索引分配
                1. 单级索引分配
                1. 多级索引分配
                1. 混合索引分配/多级混合索引分配
    1. 空闲区域管理
        1. 空闲表： 较适合连续分配
        1. 空闲链： 适合各种分配方式
        1. 位示图： 适合各种分配方式，通常将位示图加载到内存以提高速度
        1. 成组链接： 适合各种分配方式

## 5. Reliability of File System文件系统的可靠性

1. 磁盘坏块
    1. 热修复重定向
    1. 写后读校验
1. 备份
1. 数据一致性
    1. 块号一致性检查
    1. 链接数一致性检查
    1. 数据一致性控制： transaction事务与check-point检查点

## 6. Protection Mechanism of File System文件系统的保护机制

1. 保护域
1. 保护矩阵
    1. `ACL`
    1. 用户权限表
1. 分级安全管理
    1. 系统级安全管理
    1. 用户级安全管理
    1. 目录级安全管理
    1. 文件级安全管理

>阮一峰 - 权限： <https://linhaorong.top/blog/linux/file/permission/>

<!--TODO: 用户权限表-->
