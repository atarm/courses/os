# Part 04: Device Management设备管理

- [1. Agenda](#1-agenda)
    - [1.1. Main Topics](#11-main-topics)
    - [1.2. Keys for Programming](#12-keys-for-programming)
- [2. What is Device Management设备管理是什么](#2-what-is-device-management设备管理是什么)
- [3. IO硬件](#3-io硬件)
- [4. IO控制方式](#4-io控制方式)
- [5. IO系统](#5-io系统)
    - [5.1. 设备](#51-设备)
    - [5.2. SPOOLing](#52-spooling)
    - [5.3. Interrupt中断](#53-interrupt中断)
- [6. Disk Management磁盘管理](#6-disk-management磁盘管理)
    - [6.1. 磁盘结构](#61-磁盘结构)
    - [6.2. 磁盘调度](#62-磁盘调度)
    - [6.3. RAID](#63-raid)
- [7. Buffering and Caching缓冲与缓存](#7-buffering-and-caching缓冲与缓存)
    - [7.1. Difference between Buffering and Caching](#71-difference-between-buffering-and-caching)
    - [7.2. Buffering缓冲](#72-buffering缓冲)
    - [7.3. Disk Caching磁盘缓存](#73-disk-caching磁盘缓存)

## 1. Agenda

### 1.1. Main Topics

1. 设备管理面临的问题、目标和功能需求
1. 设备的组成结构和类型
1. IO的控制方式： 程序控制IO、中断控制IO、DMA控制IO、通道控制IO
1. 设备的管理和分配
1. SPOOLing原理和思想
1. 磁盘结构及其调度算法
1. buffer && cache

### 1.2. Keys for Programming

1. learn from buffer && cache
1. learn from interrupt and compare to event-driven
1. learn from SPOOLing
1. learn from disk scheduling

## 2. What is Device Management设备管理是什么

1. 设备管理面临的问题：
    1. 种类多样、操作各异 = > 如何有效地统一管理？如何有效地提供用户使用？
    1. 数量有限 = > 如何有效地共享利用？
1. 设备管理的目标：
    1. 有效： 有效地选择、分配、控制IO设备
    1. 高效： 提高使用效率、均衡设备负载、最大限度发挥设备能力
    1. 友好： 统一的接口
    1. 可靠： IO管理软件层次结构，性能可靠，易于维护
1. 设备管理的功能需求：
    1. device allocate设备分配
    1. device control and monitor设备控制和监视
    1. data transfer数据传输
    1. `cache` and `buffer`

## 3. IO硬件

1. 架构类型
    1. 总线结构： PCI(Peripheral Component Interconnect), AGP(Accelerated Graphics Port), PCI Express(Peripheral Component Interconnect Express)
    1. 主机结构： 单独的IO处理器
1. 设备类型
    1. 按信息交换单位分类
        1. block device块设备
        1. character device字符设备
    1. 按共享属性分类
        1. 独占设备
        1. 共享设备
        1. 虚拟设备
1. 设备控制器

## 4. IO控制方式

1. `Programmed`程序控制IO
1. `Interrupt-Driven`中断控制IO
1. `Direct Memory Access`DMA控制IO
1. `Channel`通道控制IO

> Linux Network IO Model、Socket IO Model - select、poll、epoll: <https://www.cnblogs.com/littlehann/p/3897910.html>

1. `programmed` vs `interrupt-driven`： 数据是否准备好的状态由谁负责？
    + `programmed`： 进程轮询，轮询期间占用`CPU`
    + `interrupt-driven`： `IO device`报告给`CPU`，发出IO指令至数据准备好期间释放`CPU`
1. `interrupt-driven` vs `DMA`： 数据传输是否经过`CPU`？
    + `interrupt-driven`： 数据从`IO device`经过`CPU`再到`main memory`
    + `DMA`： 数据直接从`IO device`至`main memory`
1. `DMA` vs `channel`： 是否有自己的指令集？
    + `DMA`： 没有自己的指令集
    + `channel`： 有自己的指令集，`channel`实际上是一个专用于`IO`的`CPU`

![should not seen](./.assets/diagram/io_controls.svg)

> The first use of channel I/O was with the IBM 709 vacuum tube mainframe, whose Model 766 Data Synchronizer was the first channel controller, in 1957.
>
> However, with the rapid speed increases in computers today, combined with operating systems that **don't 'block' when waiting for data** , channel controllers have become correspondingly less effective and are not commonly found on small machines.

`programmed`, `interrupt-driven`, `DMA`三者之间是质的飞跃，`DMA`和`channel`两者之间是量的进步

## 5. IO系统

### 5.1. 设备

1. Device Independence设备无关性/设备独立性： 进程独立于具体使用的物理设备
    + 提高设备分配的灵活性
    + 易于实现IO重定向

### 5.2. SPOOLing

1. `SPOOLing`的功能
    1. 缓和主机与低速`IO device`的速度差距
    1. 将独占设备虚拟成虚拟设备，达到共享设备的效果
1. 核心技术
    1. `virtualization`虚拟化： 将物理设备虚拟成逻辑设备，进而实现虚拟共享
    1. `buffering`缓冲： buffer存储于`main memory`或`secondary storage`中
    1. `queue`队列： 对请求排队列

### 5.3. Interrupt中断

1. 中断是什么： 指由于接收到源自外部硬件（相对于`CPU`和`main memory`）的异步信号或源自内部硬件（`CPU`和`main memory`）/软件的同步信号，而进行相应的硬件和软件的处理机制
1. 中断的用途： 中断在计算机多任务处理，尤其是实时系统中尤为重要，这样的系统，包括运行于其上的操作系统，被称为`interrupt-driven`
    1. 并行/并发执行
    1. 故障报警与处理
    1. 实时处理
1. `interrupt`类型： `asynchronous interrupt`俗称"外中断"或"硬中断"，`fault`和`abort`俗称"内中断"，`trap`和`programmed`俗称"软中断"（由软件特意引起的、通过`INT xx`指令触发的），在`Linux`系统中`trap`为`INT 0x80`，在`Linux`系统中`programmed`用于实现硬中断的下半部（上半部关闭中断，下半部打开中断、用于执行长时间的指令）
    1. **`interrupt`中断/`asynchronous interrupt`异步中断** ：由外部硬件（`CPU`和`main memory`以外的硬件）通过中断控制器传递信号到`CPU`芯片针脚
        1. **`maskable interrupt`可屏蔽中断** ： 信号通过`CPU`芯片的`INTR`针脚传递
        1. **`non-maskable interrupt`不可屏蔽中断** ： 信号通过`CPU`芯片的`NMI(Non-Maskable Interrupt)`针脚传递，用于紧急的、必须处理的故障，如：电源掉电
    1. **`exception`异常/`synchronous interrupt`同步中断**
        1. **`processor detected`**： 由`CPU`或`main memory`触发的
            1. **`fault`故障** ： 可能可以被中断处理程序修正的错误情况，如：除数为零错误、越界错误、缺段异常、缺页异常等
            1. **`trap`陷阱** ： 执行一条指令的结果，典型用途：实现`system call`从用户态切换到核心态、断点调试
            1. **`abort`终止** ： 不可修正的致命错误，典型的是一些硬件错误，比如`DRAM`或`SRAM`位校验错误
        1. **`programmed`** ： 也称为`software interrupt软件中断`，通信进程间用于模拟硬中断的一种信号通信方式
1. `interrupt`优先级
1. `interrupt`执行过程

>软中断和硬中断： <https://www.jianshu.com/p/52a3ee40ea30>

## 6. Disk Management磁盘管理

### 6.1. 磁盘结构

![disk_mechanism](./.assets/image/disk_mechanism.jpg)

![hard_disk](./.assets/image/hard_disk.jpg)

1. 磁盘的硬件结构
    1. `header`磁头
    1. `platter`盘片
    1. `cylinder`柱面
    1. `track`磁道： 自外向内编号
    1. `sector`扇区
        + 旧式磁盘每个磁道扇区数相同 = > 扇区的磁密度不同
        + 新式磁盘每个扇区磁密度相同 = > 磁道的扇区数不同
1. 磁盘是半随机访问设备，支持随机读写，但随机读写性能远低于顺序读写
1. 磁盘的地址结构：
    + 旧式磁盘： 3维寻址 = > 柱面（磁道）号、磁头（盘面）号、扇区号
    + 新式磁盘： 线性寻址 = > 扇区号
    + 兼容性： 现代磁盘控制器中的地址翻译器将3D寻址参数翻译为线性寻址参数
1. 磁盘引导块
1. 磁盘类型
    1. 硬盘和软盘
    1. 单片盘和多片盘
    1. 固定头磁盘和活动头磁盘： 固定头磁盘只是概念产品 :question:
1. 磁盘的访问时间： seek time + rotational delay time + access time

```math
\text{T} = T_s + \frac{1}{2r} + \frac{b}{rN}\\
\text{r: rotational speed, b: bytes to access, N: bytes in one track}
```

>磁盘I/O那些事： <https://tech.meituan.com/2017/05/19/about-desk-io.html>

### 6.2. 磁盘调度

1. scheduling according to requestor根据请求者的信息进行调度
    1. `RSS`(Random Scheduling)随机算法
    1. `PRI`(Priority By Process)进程优先级优先算法
    1. `FCFS`(First Come First Served)/FIFO(First In First Out)先来先服务
    1. `LCFS`(Last Come First Served)/LIFO(Last In Fisrt Out)后来先服务
1. scheduling according to requested item根据请求项的信息进行调度
    1. `SSTF`(Shortest Seek Time First)最短寻道时间优先
    1. `SCAN`类
        1. `SCAN`扫描算法/电梯算法
        1. `C-SCAN`(Circular SCAN)循环扫描算法
        1. `N-Step-SCAN`
            + N值很大时，`N-Step-SCAN`接近于`SCAN`
            + N = 1时，`N-Step-SCAN`为`FCFS`
        1. `FSCAN`： N = 2时的`N-Step-SCAN`

### 6.3. RAID

>RAID ("Redundant Array of Inexpensive Disks" or "Redundant Array of Independent Disks) is a data storage virtualization technology that combines multiple physical disk drive components into one or more logical units for the purposes of data redundancy, performance improvement, or both.

1. standard levels标准层级
    1. 简单并行访问： 非冗余，适用于对访问速度要求高、数据可靠要求低的场景
        1. `RAID0`
    1. 镜像并行访问： 冗余，适用于数据可靠要求高的场景
        1. `RAID1`： 50%利用率
    1. 位校验并行访问： 位校验冗余，适用于IO请求量大的场景
        1. `RAID2`
        1. `RAID3`
    1. 块校验并行访问： 块校验冗余，适用于访问速度要求高、访问数据量大、IO操作密集型的场景
        1. `RAID4`
        1. `RAID5`
        1. `RAID6`
1. nested/hybrid levels混合层级
    1. `RAID 0+1`/`RAID 01`
    1. `RAID 1+0`/`RAID 10`
    1. `JBOD RAID M+N`/`JOBD RAID MN`： JBOD(Just a Bunch Of Disks)

## 7. Buffering and Caching缓冲与缓存

### 7.1. Difference between Buffering and Caching

|aspect|buffering|caching|
|--|--|--|
|basic|Buffer stores data till it is processed|Caching fastens the data access speed of repeatedly used data|
|storage|Buffer stores original data |Bache stores copy of the data.|
|policy|First in First out|Least recently used, and so on|

> Difference between Buffering and Caching in OS: <https://www.geeksforgeeks.org/difference-between-buffering-and-caching-in-os/>

### 7.2. Buffering缓冲

1. 引入缓冲的目的
    1. 缓解速度不匹配
    1. 解决数据处理单位与传输单位不匹配的问题： 传输数据块，处理记录项
    1. 减少中断次数
    1. 提高`CPU`和`IO device`的并行度 = = > 提高`CPU`利用率
1. 内存缓冲区的结构类型
    1. single buffer单缓冲
    1. double buffer双缓冲
    1. circular buffer循环缓冲
    1. buffer pool缓冲池

### 7.3. Disk Caching磁盘缓存

1. 引入缓存的目的： 基于局部性原理，缓解`main memory`与`secondary storage`速度不匹配
1. 数据交付方式：
    1. 数据交付
    1. 指针交付
1. 置换算法
    1. `LRU`
    1. `LFU`
    1. `clock`
1. 磁盘缓存置换与虚拟内存置换、高速缓存置换的不同 = = > **磁盘缓存对数据一致性要求更高**
    1. 磁盘缓存置换与虚拟内存置换的区别
        1. 虚拟内存逻辑上是内存，磁盘缓存逻辑上是磁盘
        1. 虚拟内存存储的是动态数据，磁盘缓存存储的是持久化数据
        1. 虚拟内存的内存区不一定全是外存交换区的copy，磁盘缓存的内存区是外存（磁盘）的copy
    1. 磁盘缓存与高速缓存的区别： 高速缓存存储的是动态数据，磁盘缓存存储的是持久化数据
    1. 磁盘缓存数据一致性 = > 周期性写回
