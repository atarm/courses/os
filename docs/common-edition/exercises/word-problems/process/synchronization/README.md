# 进程同步

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [车站售票厅](#车站售票厅)

<!-- /code_chunk_output -->

## 车站售票厅

某车站售票厅，任何时刻最多可容纳20名购票者进入，当售票厅中少于20名购票者时，则厅外的购票者可互斥进入售票厅取号，否则需在外面等待。购票者取完号后，按先来先服务原则排队等待服务。售票厅内有3个售票窗口，分别有一位售票员服务。

分别在购票者和售票员的角度，如何用PV操作管理这些"并发"的过程？应怎样定义信号量，写出信号量的初值以及信号量各种取值的含义、信号量的上限和下限（信号量的取值范围）。

解：

```cpp {.line-numbers}
semaphore mutex = 1;        //互斥进入大厅取号
semaphore hall = 20;        //大厅剩余空间
semaphore customer = 0;     //等待服务的大厅顾客数
semaphore clerk = 3;        //服务窗口

process custom{
    P(hall);
    P(mutex);
    进入大厅;
    取号;
    V(mutex);
    V(customer);
    P(clerk);               //按号码先来先服务原则等待服务
    接受服务;
    V(hall);
}

process service{
    P(customer);
    提供服务;
    V(clerk);
}
```
