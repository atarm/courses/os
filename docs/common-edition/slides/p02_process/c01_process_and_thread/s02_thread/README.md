---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Thread_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Thread

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [进程VS线程](#进程vs线程)
2. [线程的实现方式](#线程的实现方式)

<!-- /code_chunk_output -->

---

1. 1950s mid: prototype concept of `process`
    1. 1961: `ctss(Compatible Time-Sharing System)`
    2. 1964: `ibm os/360`
    3. 1964: `multics`, a more sophisticated `process` concept
    4. 1969: `unix`, mature `process` model
2. 1980s mid: `thread`
3. 1990s late: `hyper-threading`, `smp(Symmetric Multi-Processing，对称多处理器)`(`multi-cores`, `multi-processors` etc.)

---

![width:500](./.assets/image/smp_multi-processor_architecture.jpg)
![width:500](./.assets/image/smp_multi-core_architecture.jpg)

---

1. 引入`process`的目的是为了使并发执行的程序能独立地运行，从而提高资源利用率和系统吞吐量
2. `process`是资源分配和调度的基本单位
3. 引入`thread`的目的
    1. 适应`smp`提高进程内的并行性
    2. 满足`process`内多个执行流的需要，例如：`GUI`执行流和`logic`执行流
    3. 减少调度时的时空开销，提高系统并发性能

>[wiki. Symmetric multiprocessing.](https://en.wikipedia.org/wiki/Symmetric_multiprocessing)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程VS线程

---

1. `process`是资源分配的基本单位，`thread`是调度的基本单位
2. 一个`thread`代表一个执行流
3. `thread`仅包含保证其能独立运行的必要资源（`tcb` + `stack`）
4. 一个`process`可以包含多个`thread`
5. 同一`process`内的线程调度不涉及进程调度
6. 同一个`process`的所有`thread`在同一个虚拟地址空间中、共享该`process`的资源
    1. 线程间通信简单
    2. `multi-thread programming`的隔离性不如`multi-process programming`

---

### Thread Control Block线程控制块

1. identifier标识符
2. context上下文
    - `general registers`通用寄存器
    - `PC(Program Counter)`程序计数器
    - `PSW(Program Status Word)`程序状态寄存器
    - `SP(Stack Pointer)`栈指针寄存器
3. scheduling information调度信息
    1. priority优先级

<!--TODO:-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 线程的实现方式

1. `kst(Kernel Supported Thread, 内核级线程)`：由内核支持的线程，`kernel`感知`thread`的存在
2. `ult(User Level Thread, 用户级线程)`：也称为`fiber纤程`，由用户程序支持的线程，`kernel`不能感知`thread`的存在，需要运行时系统/运行时环境的支持
3. 组合方式

>[wiki. Fiber (computer science).](https://en.wikipedia.org/wiki/Fiber_(computer_science))

---

### `fiber纤程` VS `coroutine协程`

1. `coroutine`: allow execution to be suspended and resumed
2. `fiber`是`os-level操作系统层面`的概念, `coroutine`是`language-level编程语言层面`的概念
3. `fiber`更强调`scheduling`，`coroutine`更强调`yield`
4. `coroutine`可通过`fiber`、`kst thread`等方式实现

>[wiki. coroutine.](https://en.wikipedia.org/wiki/Coroutine)

---

### `kst`的优缺点

1. 优点：
    1. 在多处理机系统中，内核可调度同一进程的多个线程
    2. 当一个线程阻塞了，内核可调度其他线程(同一或其他进程)
    3. 线程的切换（相对于进程）速度快、开销小
    4. 内核本身可采用多线程技术，提高执行速度和效率
1. 缺点：
    1. 相对于`ult`，开销较大

---

### `ult`的优缺点

1. 优点
    1. 线程切换不需要转换到内核态
    2. 调度算法可以是进程专用
    3. 线程的实现与`os`无关
2. 缺点
    1. 一个`thread`调用了`os`的`system call`，会阻塞整个进程
    2. 无法利用多处理器系统的优势
    3. 内核的调度仍然是基于进程的

---

### 组合方式

1. 多对一：`solaris green threads`, `gnu portable threads`
2. 一对一：`os/2`, `windows`, `linux`, `solaris 9 and later`
3. 多对多：`before solaris 9`

---

![image-20240316105530594](./.assets/image/image-20240316105530594.png)

---

![image-20240316105845245](./.assets/image/image-20240316105845245.png)

---

![image-20240316105951233](./.assets/image/image-20240316105951233.png)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
