---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Process_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Process

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [程序的执行](#程序的执行)
2. [进程](#进程)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 程序的执行

<!-- ---

### Precedence Graph前趋图

1. 有向无循环图，用于描述进程之间执行的先后顺序
2. 结点表示进程或程序段，有向边表示前趋关系 -->

---

### 单道串行执行

1. 只有一个程序加载到内存中，执行完成后，才能装入另一个程序执行
2. 特点：
    1. 连续性
    2. 封闭性
    3. 可重现性

---

### 多道并发执行

1. 多个程序加载到内存中，并发执行，共享系统资源
2. 特点：
    1. 间断性
    2. 失去封闭性：程序间可能相互影响
    3. 不可重现性：多次执行，初始条件相同、各种输入相同，但结果可能不同

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程

---

1. 为了使并发执行的程序能独立地运行，引入`process`（`Linux`称为`task`）的概念，使用`pcb(process control block, 进程控制块)`对`process`进行描述
2. `pcb`与`process`一一对应，是`process`存在的唯一标识，常驻于内存
3. `process`是程序的一次执行，是资源分配和调度的基本单元/独立单元
4. `pcb`是管理进程的数据结构，包括进程的基本信息、运行状态、资源分配等

>[深入理解进程之数据结构篇.](https://juejin.cn/post/7015459111403782180)

---

### 进程的特征

1. 动态性：进程是程序的一次执行，有创建、运行、消亡等状态
2. 并发性：多个进程并发执行
3. 独立性：进程是资源分配和调度的基本单位
4. 异步性：按各自独立的、不可预知的速度向前推进

---

### 程序 VS 进程

1. 程序是静态的，进程是动态的
2. 进程是程序的一个实例，是程序的一次执行
3. 程序是进程的代码和字面量部分
4. 程序在外存，进程在内存

---

### 进程控制块的作用

1. 独立运行的基本单位的标识
2. 用于实现间断性运行方式
3. 提供进程控制所需的信息
4. 提供进程调度所需的信息
5. 提供进程通信所需的信息

---

### 进程控制块的信息

1. 进程标识符
    1. 外部标识符
    2. 内部标识符
2. 处理机状态/处理机上下文
    1. 通用寄存器
    2. 指令计数器
    3. 程序状态字寄存器
    4. 栈指针寄存器
    5. 其他

---

1. 进程控制信息
    1. 代码地址和数据地址
    2. 进程同步和通信机制
    4. 下一`pcb`指针
2. 进程调度信息
    1. 进程状态
    2. 进程优先级
    3. 事件信息
    4. 其他
3. 资源列表

<!--TODO:-->

---

### 进程控制块的存储结构

1. 顺序表
2. 链接队列表
3. 索引顺序表

---

![height:600](./.assets/diagram/sequence-table.jpg)

---

![height:600](./.assets/diagram/linked-queue-table.jpg)

---

![height:600](./.assets/diagram/indexed-sequence-table.jpg)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
