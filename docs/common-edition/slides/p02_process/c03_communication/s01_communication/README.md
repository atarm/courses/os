---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Process Communication_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Process Communication

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is `Process Communication`](#what-is-process-communication)
2. [节点内进程通信](#节点内进程通信)
3. [节点间进程通信](#节点间进程通信)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## What is `Process Communication`

---

1. **communication**: **_紧密协作关系_**，多个进程为了合作完成任务，必须通过 **某种通信机制** 来进行 **信息交换**

$\text{communication} \supset \text{synchronization} \supset \text{mutual exclusion}$

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 节点内进程通信

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 共享存储通信

---

<!-- 1. 基于共享存储区域的通信方式：`os`提供共享存储区域，程序员负责对共享存储区域进行设置和进程间同步等 -->
1. 共享内存通信：进程向`os`申请一块共享内存区域，将这块内存区域映射到自己的地址空间中，进程间通过读写这块共享内存区域来进行通信
2. 共享文件通信：进程通过`os`提供的文件系统接口，共享同一个文件，进程间通过读写这个共享文件来进行通信

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 管道通信

---

1. 用于连接一个读进程和一个写进程以实现它们之间通信
2. 管道分为
    1. 匿名管道，也称为`pipe`
    2. 命名管道，也称为`fifo`
<!-- 2. 管道机制的协调能力：互斥、同步、对方是否存在 -->

<!--TODO:

add `anonymous pipe` and `named pipe`

-->

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 消息传递通信

---

1. 通信的两个进程之间通过`os`提供的消息传递机制来进行通信
2. 将数据封装在格式化的`message`中，通过`os`提供的`send`和`receive`等系统调用来发送和接收消息
3. 通信方式：
    1. 直接通信：发送方和接收方直接通信，`send(receiver, message)`, `receive(sender, message)`
    2. 间接通信：通过共享中间实体（信箱）进行通信，`send(mailbox, message)`, `receive(mailbox, message)`

<!--TODO:

add `message queue`, `mailbox`, `message passing`

-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 节点间进程通信

---

1. `socket`
3. `HTTP`/`HTTPS`
2. `RPC(Remote Procedure Call, 远程过程调用)`: `JMS`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
