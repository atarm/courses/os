---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Process Scheduling_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Process Scheduling

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [调度的层次](#调度的层次)
2. [进程调度概述](#进程调度概述)
3. [经典调度算法](#经典调度算法)
4. [实时调度算法](#实时调度算法)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 调度的层次

---

1. `long-term scheduling长程调度`/`job scheduling作业调度`：从外存中处于后备队列中的作业调入内存，并为它们创建进程并分配必要的资源，然后，将新创建的进程插入就绪队列中<!--TODO: 主要用于多道批处理系统中-->
2. `medium-term scheduling中程调度`/`memory swapping内存交换`：将暂不运行的进程从内存中换出到外存，以释放内存空间，或将外存中的进程换入内存，以使其运行。
3. `short-term scheduling短程调度`/`process scheduling进程调度`/`context scheduling上下文调度`：根据进程的状态和调度算法，从就绪队列中选择一个进程，将`cpu`分配给它，使其运行。

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程调度概述

---

### 进程调度的任务

1. 按某种调度算法选择进程
2. `switch()`
3. 将`CPU`分配给选中的进程

---

### 进程调度机制

1. 排队器：用于将进程插入相应的就绪队列
2. 分派器：用于将选定的进程移出就绪队列
3. 上下文切换器：进行新旧进程之间的`context switch`

---

![width:1120](./.assets/image/process_scheduling_mechanism.png)

---

### 进程调度的方式

1. `non-preemptive不可剥夺式/非抢占式`：一旦把`cpu`分配给某进程后，便让该进程一直执行，直至该进程退出或被阻塞时，才将`cpu`分配给其他进程，不允许其他进程抢占已经分配出去的`cpu`
2. `preemptive可剥夺式/抢占式`：允许调度程序根据某种原则，暂停某个正在执行的进程，将已分配给该进程的`cpu`重新分配给另一进程

---

#### 抢占式调度常见原则

1. 优先级原则：允许优先级高的新到进程抢占当前进程的`cpu`
2. 时间片原则：各进程按时间片运行，当一个时间片用完后，停止该进程的执行而重新进行调度
3. 短任务优先原则：短任务可以抢占当前较长任务的处理器

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 进程调度算法的目标

---

#### 量化评价指标

1. $\text{CPU利用率} = \frac{CPU有效工作时间}{CPU有效工作时间 + CPU空闲等待时间}$
2. $\text{响应时间} = \text{首次响应时刻} - \text{到达时刻}$
3. $\text{吞吐量} = \frac{\text{完成进程数}}{\text{总时间}}$
    1. $\text{QPS} = \frac{\text{number of queries}}{\text{times}}$
    2. $\text{TPS} = \frac{\text{number of transactions}}{\text{times}}$

---

1. $\text{周转时间} = \text{完成时刻} - \text{到达时刻} = \text{等待时间} + \text{运行时间}$
2. $\text{带权周转时间} = \frac{\text{周转时间}}{\text{运行时间}}$
3. $\text{平均周转时间} = \frac{\sum \text{周转时间}}{\text{进程数}}$
4. $\text{带权平均周转时间} = \frac{\sum \text{带权周转时间}}{\text{进程数}}$

<!-- 1. $T_{response} = T_{first-run} - T_{arrival}$ -->

---

#### 进程调度算法的公共目标

1. 资源利用率
2. 公平性：各进程都获得合理的`cpu`时间，避免饥饿现象
3. 平衡性：主机系统中各种资源的平衡分配
4. 策略优先性：对所制定的策略有较高的优先级

---

#### 批处理系统进程调度算法的特殊目标

1. 平均周转时间短
2. 系统吞吐量大

---

#### 分时系统进程调度算法的特殊目标

1. 响应时间短
2. 响应时间均衡：响应时间的快慢应用户请求服务的复杂度相适应

---

#### 实时系统进程调度算法的特殊目标

1. 截止时间的保证
2. 可预测性

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 经典调度算法

---

1. `FCFS`(First Come First Served, 先来先服务)
2. `SPF`(Shortest Process First, 短进程优先)/`SPN`(Shortest Process Next)/`SJF`(Shortest Job First, 短作业优先)
3. `PR`（Priority, 优先级调度）
4. `HRRN`(Highest Response Ratio Next, 高响应比优先)
5. `RR`(Round Robin，时间片轮)
6. `MQ`(Multilevel Queue, 多级队列)
7. `MFQ`(Multilevel Feedback Queue, 多级反馈队列)

<!-- 3. **`SRTF`(Shortest Remaining Time First, 最短剩余时间优先)** -->
---

### `FCFS`(First Come First Served, 先来先服务)

1. 按照到达的先后次序进行调度
2. 每次调度从就绪队列中选择一个最先到达的进程，分配处理器给它
3. `non-preemptive`调度算法

---

1. $T_1$运行时间24，$T_2$运行时间3，$T_3$运行时间3
2. 假设按$T_1, T_2, T_3$次序到达
    1. 平均等待时间 = (0 + 24 + 27)/3 = 17
    2. 平均周转时间 = (24 + 27 + 30)/3 = 27
3. 假设按$T_2, T_3, T_1$次序到达
    1. 平均等待时间 = (6 + 0 + 3)/3 = 3
    2. 平均周转时间 = (30+ 3 + 6)/3 = 13

---

### `SPF`(Shortest Process First, 短进程优先)

1. 从就绪队列中选择一个预估运行时间最短的进程
2. 既可以是`non-preemptive`，也可以是`preemptive`
3. `preemptive`时，抢占发生在有比当前进程剩余时间片更短的进程到达时，此时，也称为`SRTF`(Shortest Remaining Time First，最短剩余时间优先)
4. 对于一组指定的进程而言，`SPF`算法的平均等待时间（平均周转时间）最短

---

#### `SPF`算法的缺点

1. 只能估算进程的运行时间（估值可能不准确），通常用于作业调度
2. 对长进程不利
3. 人机交互难于实现
4. 未考虑进程的紧迫程度

---

进程|到达时间|运行时间
--|--|--
P1|0|7
P2|2|4
P3|4|1
P4|5|4

---

#### 非抢占式`SPF`

![width:1120](./.assets/image/spf_example_non-preemptive.jpg)

1. 平均等待时间 = (0 + 6 + 3 + 7)/4 = 4
2. 平均周转时间 = (7 + 10 + 4 + 11)/4 = 8

---

#### 抢占式`SPF`

![width:1120](./.assets/image/spf_example_preemptive.jpg)

1. 平均等待时间 = (9 + 1 + 0 +2)/4 = 3
2. 平均周转时间 = (16 + 5 + 1 + 6)/4 = 7

---

### `PR`（Priority, 优先级调度）

1. 基于进程的紧迫程度，由外部赋予进程某个优先级
2. 每个进程都有一个优先级，优先级高的进程先执行
2. 既可以是`non-preemptive`，也可以是`preemptive`
3. `preemptive`时，抢占发生在有比当前进程优先级更高的进程进入`ready queue`时

---

#### 优先级类型

1. 静态优先级：创建进程时确定优先级，在进程的整个运行期间保持不变
    1. 优点：简单易行，系统开销小
    2. 缺点：饥饿现象，可能会出现优先级低的进程长期没有被调度的情况
2. 动态优先级：创建进程时先赋予其一个优先权，然后其值随进程的推进或等待时间的增加而改变

---

进程|优先级|运行时间
--|:--:|:--:
P1|3|10
P2|1|1
P3|3|2
P4|4|1
P5|2|5

---

#### 非抢占式`PR`

![width:1120](./.assets/image/pr_non-preemptive.jpg)

1. 平均等待时间 = (0 + 5 + 16+18+1)/5 =8
2. 平均周转时间 = (16 + 1 + 18+19+6)/5 = 12

---

### `HRRN`(Highest Response Ratio Next, 高响应比优先)

<!-- 1. 优先级由等待时间和服务时间的比值决定 -->

1. `HRRN`是`PR`的一个特例，优先级由等待时间和要求服务时间共同决定，通常用于作业调度
2. 优先级计算公式：$R_p = \frac{\text{等待时间} + \text{要求服务时间}}{\text{要求服务时间}} = \frac{\text{响应时间}}{\text{要求服务时间}}$
    1. 如果等待时间相同，则选择要求服务时间较短的进程，类似于`SPF`
    2. 如果要求服务时间相同，则选择等待时间较长的进程，类似于`FCFS`
3. 优点：长进程随着其等待时间的增加优先级提高，不会产生饥饿现象
4. 缺点：每次调度时，都需要更新就绪队列中全部进程的响应比，系统开销较大

---

### `RR`(Round Robin，时间片轮)

1. 为每个进程分配不超过一个时间片的CPU，时间片用完后，该进程将被抢占并插入就绪队列末尾，循环执行
2. 假定就绪队列中有n个进程、时间片为q, 则每个进程每次得到1/n的、不超过q单位的成块CPU时间，没有任何一个进程的等待时间会超过(n-1)*q单位
3. 类似于`FCFS`，但，`RR`是基于时间片轮转的`preemptive`调度算法
4. `RR`的平均周转时间比`SPF`长，但响应时间要短，适合于人机交互场景

---

#### 时间片大小的确定

1. 时间片过大：类似于`FCFS`
2. 时间片过小：进程切换频繁，系统开销大
3. 时间片大小选择：与系统的响应时间要求、就绪队列中的进程数目、系统的处理能力相关
4. 基本准则：$\text{时间片} \gt \text{上下文切换时间} * 10$

---

![width:1000](./.assets/image/rr_time-slice.png)

---

![width:1120](./.assets/image/example_rr.jpg)

---

### `MQ`(Multilevel Queue, 多级队列)

1. 就绪队列分为多个队列，例如：前台队列和后台队列
2. 每个队列可以有自己的调度算法，例如：前台队列采用`RR`，后台队列采用`FCFS`
3. 调度在队列间和队列内进行
    1. 固定优先级调度，例如，前台运行完后再运行后台，有可能产生饥饿
    2. 给定时间片调度，每个队列得到一定的`cpu`时间，进程在给定时间内执行，例如，80%的时间执行前台的`RR`，20%的时间执行后台的`FCFS`

---

### `MFQ`(Multilevel Feedback Queue, 多级反馈队列)

1. 如果进程所需的运行时间未知或不可预估，则`SPF`以及任何基于预估运行时间的调度算法都无法使用
2. `MFQ`设置多个新绪队列
    1. 每个队列有不同的时间片大小，优先级不同
    2. 每个队列采用`FCFS`或其他调度算法
    3. 按照队列优先级调度，进程从高优先级队列开始执行，如果时间片用完，进程将被移到下一个队列
3. 优点：
    1. 不必事先知道各种进程所需的运行时间
    2. 可以满足各种类型进程的需要

---

![mfq](./.assets/image/mfq.jpg)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 实时调度算法

---

1. 实时调度算法应满足实时任务对响应时间和截止时间的要求，特别是截止时间的要求
2. 实时调度的一些条件（非充分条件）
    1. 必要信息：到达时间、开始截止时间和完成截止时间、处理时间、资源要求、优先级等
    2. 硬件能力：系统处理能力强，应大于实时任务的总需求
    3. 抢占调度：一般采用抢占式调度机制
    4. 快速切换：快速的中断响应机制、快速的任务分派机制、快速的`context switch`机制

---

### 实时调度的类型

1. 根据实时任务的性质
    1. `HRT`调度算法
    2. `SRT`调度算法
2. 根据调度方式
    1. 非抢占式调度算法
    2. 抢占式调度算法

---

### 非抢占式实时调度算法

1. 非抢占式轮转调度算法
    1. 响应时间：数秒至数十秒
    2. 可用于要求不太严格的实时系统
2. 非抢占式优先级调度算法
    1. 响应时间：数百毫秒至数秒
    2. 可用于有一定要求的实时系统

---

### 抢占式实时调度算法

1. 基于时钟中断的抢占式优先级调度
    1. 响应时间：几十毫秒至几毫秒
    2. 可用于大多数实时系统
2. 基于立即抢占的优先级调度
    1. 响应时间：几毫秒至几百微秒
    2. 可用于有严格时间要求的实时系统

---

### `EDF`(Earliest Deadline First, 最早截止时间优先)

1. 截止时间越早，优先级越高
2. `EDF`即可用于`preemptive`，也可用于`non-preemptive`
<!-- 3. `non-preemptive`适用于非周期性任务，`preemptive`适用于周期性任务 -->

---

#### `EDF Non-Preemptive`

![width:1120](./.assets/image/edf_nonpreemptive.jpg)

---

#### `EDF Preemptive`

![height:520](./.assets/image/edf_preemptive.jpg)

---

### `LLF`(Least Laxity First, 最小松弛度优先)

1. 紧急程度越高（松弛度越低），优先级越高
2. $\text{松弛度} = \text{截止时间} - \text{运行时间} - \text{当前时间}$
3. `LLF`主要用于`preemptive`

---

1. 两个周期性实时任务A和B，它们必须在下一周期任务到达前完成
2. 任务A要求每20ms执行一次，执行时间为10ms
3. 任务B要求每50ms执行一次，执行时间为25ms

---

![width:1120](./.assets/image/llf_example_01.jpg)

---

1. $A_1 = 20 \text{ms} - 10 \text{ms} - 0 \text{ms} = 10 \text{ms}$
2. $B_1 = 50 \text{ms} - 25 \text{ms} - 0 \text{ms} = 25 \text{ms}$
3. $A_1$在10ms后完成
4. $B_1 = 50 \text{ms} - 25 \text{ms} - 10 \text{ms} = 15 \text{ms}$
4. $A_2 = 40 \text{ms} - 10 \text{ms} - 10 \text{ms} = 20 \text{ms}$
5. 在30ms时，$A_2$的松弛度变为0ms，$B_1$的松弛度变为15ms（50 - 5- 30），调度程序抢占$B_1$调度$A_2$执行
6. ...

---

![width:1120](./.assets/image/llf_example_02.jpg)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
