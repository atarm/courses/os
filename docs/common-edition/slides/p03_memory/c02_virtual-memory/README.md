---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Virtual Memory_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Virtual Memory

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is `Virtual Memory`](#what-is-virtual-memory)
2. [`overlay` and `swapping`覆盖与交换](#overlay-and-swapping覆盖与交换)
3. [请求分页内存管理系统](#请求分页内存管理系统)
4. [页面置换算法](#页面置换算法)
5. [请求分段内存管理系统](#请求分段内存管理系统)
6. [请求段页式系统](#请求段页式系统)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## What is `Virtual Memory`

---

### 物理内存分配存在的问题

1. 某些进程在内存中处于非执行状态而占用着内存（主要是阻塞状态或挂起状态）
2. 一些程序因内存不够而无法装载到内存运行

<!-- ---

1. 一次性：程序被一次性全部装入内存
2. 驻留性：进程一直驻留在内存
3. 一次性和驻留性使许多在程序运行中不用或暂不用的程序（数据）占据了大量的内存空间，使得一些需要运行的作业无法装入运行 -->

---

### 程序的局部性原理

1. 1968年，`P. denning`提出"程序的局部性原理"
2. 顺序执行：程序执行时，除了少部分的转移和过程调用外，在大多数情况下仍然是顺序执行的
3. 有限嵌套：过程调用将会使程序的执行轨迹由一部分区域转至另一部分区域，过程调用的深度一般小于5，程序将会在一段时间内都局限在这些过程的范围内运行
4. 循环结构：程序中存在许多循环结构，多次执行
5. 数据访问：对数据结构的处理局限于很小的范围

---

1. 时间局部性：一个内存单元被访问了，在不久的将来它可能再次被访问
2. 空间局部性：一个内存单元被访问了，在不久的将来它附近的内存竟可能被访问

---

1. 基于局部性原理，进程运行时，没有必要全部装入内存，仅须将那些当前要运行的内容先装入内存便可运行
2. 虚拟存储系统的定义：具有请求调入功能和置换功能，能从逻辑上对内存容量加以扩充的一种存储器系统
3. 虚拟存储系统的目标：最大逻辑内存容量由内存容量和外存容量之和所决定，运行速度接近于内存速度、成本接近于外存

---

### 虚拟存储器的特征

1. 多次性：程序中的代码和数据允许被分成多次调入内存运行
2. 对换性：进程运行时无须常驻内存
3. 虚拟性：从逻辑上扩充了内存容量，使用户看到的内存容量远大于实际内存容量

---

1. 地址转换：将逻辑地址（虚拟地址）转换为物理地址（真实地址）
2. 虚拟扩充：利用外存空间，从逻辑上扩充内存容量
<!-- 3. 内存保护：保护系统进程和用户进程的内存空间 -->

---

### 虚拟存储器的实现方法

1. 请求分页系统
    1. 硬件：页表、地址转换、缺页中断
    2. 软件：请求调页算法、页面置换算法
2. 请求分段系统
    1. 硬件支持：段表、地址变换机构、缺段中断
    2. 软件支持：请求调段算法、段置换算法
3. 请求段页式系统

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `overlay` and `swapping`覆盖与交换

---

### `overlay覆盖`

1. 程序运行时
    1. 只在内存中保留那些需要的指令和数据
    2. 程序的不同部分在内存中相互覆盖替换
2. 缺点
    1. 覆盖结构的程序设计复杂
    2. 由程序员定义覆盖结构
3. 应用于早期的操作系统

---

![height:600](./.assets/image/overlay.jpg)

---

### `swapping对换/交换`

1. 将内存中的某些暂时不能执行的内存空间换出到外存，以便为新进程腾出内存空间
2. 对换的类型
    1. `整体对换`/`进程对换`：整个进程换入/换出，主要应用于批处理系统的中程调度
    2. `部分对换`/`页面对换`/`分段对换`：对换是以"页"或"段"为单位进行的，目前，主要应用于虚拟内存系统

---

![height:600](./.assets/image/swapping.jpg)

---

#### 对换技术的组成部分

1. 对换空间管理
2. 换出
3. 换入

<!-- ---

##### 对换空间管理

1. 目标
    1. 提高进程换入和换出的速度
    2. 提高文件存储空间的利用率次之
1. 对换空间管理的数据结构
    1. 用于记录外存对换区中的空闲盘块的使用情况
    1. 与动态分区分配方式相似
    1. 空闲分区表/空闲分区链：包含对换区首址及大小
1. 对换空间的分配与回收：与动态分区方式的内存分配与回收方法相似 -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 请求分页内存管理系统

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 请求分页中的硬件支持

---

#### 页表

1. 状态位P：指示该页是否在内存
2. 访问字段A：记录该页在一段时间内被访问的次数
3. 修改位M：也称脏位，标志该页是否被修改过
4. 外存地址：指示该页在外存中的地址（物理块号）

$页号，物理块号，状态位，访问字段，修改位，外存地址$

---

#### 缺页中断

1. 在指令执行期间产生和处理中断信号
2. 一条指令在执行期间，可能产生多次缺页中断

---

#### 地址转换

1. 与分页内存管理方式类似

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 请求分页中的内存分配

---

1. 最小物理块数的确定：保证进程正常运行所需的最小物理块数
2. 物理块的分配策略
    1. 固定分配、局部置换
    2. 可变分配、全局置换
    3. 可变分配、局部置换
3. 物理块分配算法
    1. 平均分配算法
    2. 比例分配算法
    3. 优先级分配算法

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 页面调入策略

---

#### 调入页面的时机

1. 预调页策略：预先调入一些页面到内存
2. 请求调页策略：发现需要访问的页面不在内存时，调入内存

---

#### 调入页面的位置

1. 如系统拥有足够的对换区空间，全部从对换区调入所需页面
2. 如系统缺少足够的对换区空间，凡是不会被修改的文件，都直接从文件区调入；当换出这些页面时，由于未被修改而不必再将它们重写磁盘，以后再调入时，仍从文件区直接调入
3. UNIX方式：未运行过的页面，从文件区调入；曾经运行过但又被换出的页面，从对换区调入

---

#### 调入页面的算法

1. 查找所需页在外存上的位置
2. 查找一个内存空闲块
    1. 如果有空闲块，就直接使用它
    2. 如果没有空闲块，使用页面置换算法选择一个"牺牲"内存块
    3. 将"牺牲"块的内容写到磁盘上，更新页表和物理块表
3. 将所需页读入（新）空闲块，更新页表
4. 运行进程

---

#### 缺页率

1. 访问页面成功(在内存)的次数为S
2. 访问页面失败(不在内存)的次数为F
3. 总访问次数为A=S+F
4. 缺页率为 f= F/A
5. 影响因素：页面大小、分配内存块的数目、页面置换算法、程序固有属性
6. 缺页中断处理的时间t = β×ta + (1- β)×tb

---

1. 存取内存的时间= 200 nanoseconds (ns)
2. 平均缺页处理时间 = 8 milliseconds (ms)
3. t = (1 - p) × 200ns + p × 8ms = (1 - p) × 200ns + p ×8,000,000ns = 200ns + p × 7,999,800ns
4. 如果每1,000次访问中有一个缺页中断，那么：t = 8.2 ms （41倍访存时间）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 页面置换算法

---

1. 页面置换：找到内存中不使用的一些页，换出
    1. 算法：替换策略
    2. 性能：找出一个导致最小缺页数的算法
2. 同一个页可能会被换入换出内存多次
<!-- 3. 页面置换完善了逻辑内存和物理内存的划分--在一个较小的物理内存基础之上可以提供一个大的虚拟内存 -->

---

1. 最佳置换算法（OPT）
2. 先进先出置换算法（FIFO）
3. 最近最久未使用置换算法（LRU）
4. 最少使用算法（LFU）
5. Clock置换算法
<!-- 6. 页面缓冲算法 -->

<!-- 需要一个最小的缺页率
通过运行一个内存访问的特殊序列（访问序列），计算这个序列的缺页次数 -->

---

### 最佳置换算法（OPT）

1. 被置换的页将是之后最长时间不被使用的页
2. `OPT`是一个理想的、无法实现的算法，用于评价其他算法

![width:1120](./.assets/image/image-20240419101118315.png)

---

### 先进先出置换算法（FIFO）

1. 总是淘汰最先进入内存的页面，即选择在内存中驻留时间最久的页面予以淘汰

---

1, 2, 3, 4, 1, 2, 5, 1, 2, 3, 4, 5

![image-20240419101339248](./.assets/image/image-20240419101339248.jpeg)

<!-- ---

![image-20240419101510003](./.assets/image/image-20240419101510003.png) -->

---

### 最近最久未使用置换算法（LRU）

1. 选择最近最久未使用的页面予以淘汰

![image-20240419101623810](./.assets/image/image-20240419101623810.png)

<!-- ---

#### LRU算法的硬件支持

1. 寄存器：为内存中的每个页面设置一个移位寄存器
    1. 被访问的页面对应寄存器的Rn-1位置为1，定时右移
    2. 具有最小数值的寄存器所对应的页面为淘汰页
2. 栈：保存当前使用的各个页面的页面号
    1. 被访问的页，移到栈顶
    2. 栈底是最近最久未使用页面的页面号 -->

---

### 最少使用算法（LFU）

1. 为内存中的每个页面设置一个移位寄存器，用来记录该页面的被访问频率
2. LFU 选择在最近时期使用最少的页面作为淘汰页

---

### Clock置换算法

1. LRU的近似算法，又称最近未用(NRU)或二次机会页面置换算法
2. 简单的Clock算法
    1. 每个页都与一个访问位相关联，初始值位0
    2. 当页被访问时置访问位为1
    3. 置换时选择访问位为0的页 ；若为1，重新置为0

---

![image-20240419102018914](./.assets/image/image-20240419102018914.jpeg)

---

### 改进型Clock算法

1. 除须考虑页面的使用情况外，还须增加置换代价
2. 淘汰时，同时检查访问位A与修改位M
    1. 第1类（A=0，M=0）：最近未被访问、未被修改，最优淘汰页
    2. 第2类（A=0，M=1）：最近未被访问、已被修改，次优淘汰页
    3. 第3类（A=1，M=0）：表示该页最近已被访问，但未被修改，该页有可能再被访问。
    4. 第4类（A=1，M=1）：表示该页最近已被访问且被修改，该页有可能再被访问
3. 置换时，循环依次查找第1类、第2类页面，找到为止

<!-- ---

### 页面缓冲算法PBA

1. 影响效率的因素
    1. 页面置换算法
    2. 写回外存的频率
    3. 读入内存的频率
1. 目的
    1. 显著降低页面换进、换出的频率，减少了开销
    2. 可采用较简单的置换策略，如不需要硬件支持

---

1. 实现方法：设置两个链表
    1. 空闲页面链表：保存空闲物理块
    2. 修改页面链表：保存已修改且需要被换出的页面等被换出的页面数目达到一定值时，再一起换出外存， -->

---

### 访问内存的有效时间EAT

1. 访问页在内存，且其对应页表项在快表中：EAT= λ + t
2. 访问页在内存，且其对应页表项不在快表中：EAT= λ + t + λ + t = 2(λ + t)

---

3. 访问页不在内存中
    1. 需进行缺页中断处理，有效时间可分为查找快表的时间、查找页表的时间、处理缺页的时间、更新快表的时间和访问实际物理地址的时间
    2. 假设缺页中断处理时间为ε：EAT = λ + t + ε + λ + t = ε + 2(λ + t)
    3. f为缺页率， φ为缺页中断处理时间：EAT = t + f ×(φ + t) + (1- f) × t

<!--
---

## 抖动与工作集

---

1. 如果一个进程没有足够的页，那么缺页率将很高，这将导致:
    1. CPU利用率低下
    2. 操作系统认为需要增加多道程序设计的道数
    3. 系统中将加入一个新的进程
2. 抖动(Thrashing)  ：一个进程的页面经常换入换出

---

### 处理机的利用率

![image-20240419102854405](./.assets/image/image-20240419102854405.png)

---

### 产生抖动的原因

1. 同时在系统中运行的进程太多
2. 分配给每一个进程的物理块太少，不能满足进程运行的基本要求，致使进程在运行时，频繁缺页，必须请求系统将所缺页面调入内存

抖动的发生与系统为进程分配物理块的多少有关

---

1. 进程发生缺页的时间间隔与所获得的物理块数有关
2. 根据程序运行的局部性原理，如果能够预知某段时间内程序要访问的页面，并将它们预先调入内存，将会大大降低缺页率

---

![image-20240419103243761](./.assets/image/image-20240419103243761.jpeg)

---

### 工作集

1. 工作集，指在某段时间间隔Δ里进程实际要访问页面的集合
2. 把某进程在时间t的工作集记为w(t，Δ),其中的变量Δ称为工作集的"窗口尺寸"
3. 工作集w(t，Δ)是二元函数，即在不同时间t的工作集大小不同，所含的页面数也不同；工作集与窗口尺寸Δ有关，是Δ的非降函数，即：![image-20240419103455901](./.assets/image/image-20240419103455901.png)

---

![image-20240419103528582](./.assets/image/image-20240419103528582.png)

---

### 抖动的预防方法

1. 采取局部置换策略：只能在分配给自己的内存空间内进行置换
2. 把工作集算法融入到处理机调度中
3. 利用"L=S"准则调节缺页率：L是缺页之间的平均时间，S是平均缺页服务时间，即用于置换一个页面的时间
    1. L>S，说明很少发生缺页
    2. L<S，说明频繁缺页
    3. L=S，磁盘和处理机都可达到最大利用率
4. 选择暂停进程 -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 请求分段内存管理系统

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 请求分段中的硬件支持

---

#### 段表

1. 段名
2. 段长
3. 段始址
4. 存取方式：表示段存取属性为只执行、只读或允许读/写
5. 访问字段A：记录该段在一段时间内被访问的次数
6. 修改位M：标志该段调入内存后是否被修改过
7. 存在位P：指示该段是否在内存
8. 增补位：表示该段在运行过程中是否做过动态增长
9. 外存始址：指示该段在外存中的起始地址（盘块号）

<!-- $段名 段长 段的始址 存取方式 访问字段A 修改位M 存在位P 增补位 外存始址$ -->

---

#### 缺段中断

1. 在指令执行期间产生和处理中断信号
2. 一条指令在执行期间，可能产生多次缺段中断
3. 由于段不是定长的，对缺段中断的处理要比对缺页中断的处理复杂

---

#### 地址转换

1. 若段不在内存中，则必须先将所缺的段调入内存，并修改段表，然后利用段表进行地址变换

<!--TODO: add 段的共享

-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 请求段页式系统

---

1. 请求分页系统和请求分段系统相结合，既具有请求分页系统的优点，又具有请求分段系统的优点
2. 换入换出与请求分页系统类似

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
