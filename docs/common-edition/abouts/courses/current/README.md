---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_All About This Course_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# All About This Course

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Bases](#bases)
2. [Goals](#goals)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Bases

---

1. 课程名称：操作系统Operating Systems
2. 课程代码：U0203051
3. 课程性质：专业课 必修课
4. 考核方式：非统考
5. 适用专业：21级软件工程
6. 课程学分：3
7. 课程学时：48

---

1. 先修课程：计算机组成原理/微机原理、计算机接口技术/接口与通讯、C语言、数据结构（算法与数据结构）
2. 后续课程：计算机体系结构、编译原理、嵌入式系统、分布式系统、任何编程语言和软件工程课程

---

![height:500](./.assets/image/汤小丹_计算机操作系统（慕课版）.202105_978-7-115-56115-2.png)
![height:500](./.assets/image/汤小丹_计算机操作系统实验指导（Linux版）.202112_978-7-115-58064-1.jpeg)
![height:500](./.assets/image/汤小丹_计算机操作系统习题与考研真题解析.202106_978-7-115-56347-7.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals

---

### 知识目标

1. 了解操作系统的发展历史
2. 理解操作系统的基础概念
3. 掌握操作系统的基本原理
4. 掌握操作系统的核心模块及其核心原理
5. 掌握unix-like操作系统的基本使用
6. 掌握shell命令的基本使用
7. 掌握shell编程的基本方法

---

### 能力目标

1. 具备将操作系统的基本原理应用于实际问题的能力
2. 具备将操作系统的经典问题应用于实际开发的能力
3. 具备从操作系统的角度分析和解决实际故障的能力

---

### 素养目标

1. 积极思考、主动学习
2. 系统思维、跨界思维
3. 精益求精的工匠精神

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
